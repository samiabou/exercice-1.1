package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.IOException;

/**
 * Une Abstract class concerne la désérialisation
 *
 *
 * @param <M>
 */

public abstract class AbstractStreamingImageDeserializer<M> implements ImageStreamingDeserializer<M> {
    @Override
    public void deserialize(ImageByteArrayFrame media) throws IOException {

        getDeserializingStream(media).transferTo(getSourceOutputStream());
    }
}
