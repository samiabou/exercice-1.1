package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.*;

public class ImageByteArrayFrame extends AbstractImageFrameMedia {

    ByteArrayOutputStream ToEncodeOutputStream;
    ByteArrayInputStream noEncodedInputStream;

    /**
     *
     * @param byteArrayOutputStream
     */

    public ImageByteArrayFrame (ByteArrayOutputStream byteArrayOutputStream) {
            this.ToEncodeOutputStream = byteArrayOutputStream;

    }
    @Override

    public OutputStream getEncodedImageOutput() throws IOException {
        return this.ToEncodeOutputStream;
    }

    @Override
    public InputStream getEncodedImageInput() throws IOException {
        return this.noEncodedInputStream;
    }

    public void transferTo(ByteArrayOutputStream sourceOutputStream){

    }

}

