package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 *  Super Class pour sérializer une image et la transmet par flux
 *
 *
 * @param <S>
 * @param <M>
 */
public abstract class AbstractStreamingImageSerializer<S,M> implements ImageStreamingSerializer<S,M> {

    @Override
    public  void serialize(File source, ImageByteArrayFrame media) throws IOException {
        getSourceInputStream(source).transferTo(getSerializingStream(media));
    }

    protected abstract OutputStream getSerializingStream(ImageByteArrayFrame media) throws IOException;

    protected abstract InputStream getSourceInputStream(File source) throws IOException;

    public  InputStream getSourceInputStream ( InputStream source) throws IOException{

        return null;
    }

}
