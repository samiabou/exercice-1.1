package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.ByteArrayOutputStream;
import java.io.IOException;


/**
 *  Class permet de désérialiser un flux encodé avec base64
 *
 */

public class ImageDeserializerBase64StreamingImpl extends AbstractStreamingImageDeserializer{

    ByteArrayOutputStream deserializationOutput;

    public ImageDeserializerBase64StreamingImpl(ByteArrayOutputStream deserializationOutput) {
    this.deserializationOutput = deserializationOutput;

    }

    @Override
    public void deserialize(Object media) throws IOException{
    }

    @Override
    public void deserialize(ImageByteArrayFrame media) {

    }

    @Override
    public ImageByteArrayFrame getDeserializingStream(ImageByteArrayFrame media){

        return null;
    }

    @Override
    public ByteArrayOutputStream getSourceOutputStream() {
        return this.deserializationOutput;
    }

    @Override
    public ImageByteArrayFrame getDeserializingStream(Object media){
        return null;
    }
}
