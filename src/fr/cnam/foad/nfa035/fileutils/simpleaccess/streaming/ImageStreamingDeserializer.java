package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.ByteArrayOutputStream;
import java.io.IOException;


public interface ImageStreamingDeserializer<M> {

    void deserialize(M media) throws IOException;

    void deserialize(ImageByteArrayFrame media) throws IOException;

    ImageByteArrayFrame getDeserializingStream(ImageByteArrayFrame media) throws IOException;

    ImageByteArrayFrame getDeserializingStream (Object media) throws IOException;

    ByteArrayOutputStream getSourceOutputStream();
}