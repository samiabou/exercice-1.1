package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ImageSerializerBase64StreamingImpl extends AbstractStreamingImageSerializer {

    private InputStream inputStream;


    @Override
    public OutputStream getSerializingStream(ImageByteArrayFrame media) throws IOException {
        return media.getEncodedImageOutput();
    }
    @Override
    public OutputStream getSerializingStream(Object media) throws IOException {
        return null;
    }
     public OutputStream base64StreamEncoder (File source) throws IOException{

        OutputStream toEncode = getSerializingStream(source);
         Base64OutputStream encoder = new Base64OutputStream(toEncode);
         OutputStream encoded =encoder;
         return encoded;
     }
    @Override
    public InputStream getSourceInputStream(Object source) throws IOException {

        return this.inputStream;
    }
    @Override
    protected InputStream getSourceInputStream(File source) {
        // TODO Auto-generated method stub
        return this.inputStream;
}

    @Override
    public void serialize(File source, ImageByteArrayFrame media) throws IOException {
        OutputStream encoded = base64StreamEncoder(source);
    }
}

